# Copyright (c) 2020 Marco Marinello <me@marcomarinello.it>
# This file is part of Website - Form OpenCaptcha.
#
# Website - Form OpenCaptcha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Website - Form OpenCaptcha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Website - Form OpenCaptcha.  If not, see <http://www.gnu.org/licenses/>.

import os
from PIL import Image, ImageDraw, ImageFont

APP_PATH = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

font = ImageFont.truetype(
    os.path.join(APP_PATH, "carlito", "Carlito-Regular.ttf"),
    120
)


# This function comes from
# https://github.com/mbi/django-simple-captcha/blob/master/captcha/views.py
# Copyright (c) 2008 - 2014 Marco Bonetti
# see https://github.com/mbi/django-simple-captcha/blob/master/LICENSE
def getsize(font, text):
    if hasattr(font, "getoffset"):
        return tuple([x + y for x, y in zip(font.getsize(text), font.getoffset(text))])
    else:
        return font.getsize(text)


def genimg(letters="AAAA"):
    size = getsize(font, letters)
    size = (2 * size[0], 1.4 * size[1])
    image = Image.new("RGBA", tuple(map(int, size)))
    chardraw = ImageDraw.Draw(image)
    chardraw.text((0, 0), " %s " % letters, font=font, fill="#000000")
    return image
