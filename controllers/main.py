# Copyright (c) 2020 Marco Marinello <me@marcomarinello.it>
# This file is part of Website - Form OpenCaptcha.
#
# Website - Form OpenCaptcha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Website - Form OpenCaptcha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Website - Form OpenCaptcha.  If not, see <http://www.gnu.org/licenses/>.

from cryptography.fernet import Fernet
from flectra import http
from flectra.addons.website_form.controllers.main import WebsiteForm
from flectra.exceptions import ValidationError
from flectra.http import request
from io import BytesIO
import base64, json, string, random
from . import imgutils


class WebsiteForm(WebsiteForm):
    @http.route(
        '/website/opencaptcha/getimg',
        type='http',
        auth='public',
        methods=['GET'],
        website=True,
        multilang=False,
    )
    def opencaptcha_img(self):
        mystr = "".join([
            random.choice(string.ascii_uppercase) for i in range(4)
        ])
        image = imgutils.genimg(mystr)
        out = BytesIO()
        image.save(out, "PNG")
        out.seek(0)
        ir = request.env['ir.config_parameter']
        ENC_KEY = ir._get_param("website.form.opencaptcha_secret")
        if not ENC_KEY:
            ENC_KEY = Fernet.generate_key()
            ir.sudo().set_param("website.form.opencaptcha_secret", ENC_KEY.decode("utf-8"))
        return json.dumps({
            "image": base64.b64encode(out.getvalue()).decode("utf-8"),
            "enctxt": Fernet(ENC_KEY).encrypt(mystr.encode()).decode("utf-8")
        })


    def extract_data(self, model, values):
        """ Inject openCaptcha validation into pre-existing data extraction """
        ENC_KEY = request.env['ir.config_parameter']._get_param("website.form.opencaptcha_secret")
        if model.website_form_opencaptcha:
            #opencaptcha_model = request.env['website.form.opencaptcha'].sudo()
            #recaptcha_model.validate_request(request, values)
            if "opencaptcha_validation" not in values:
                raise ValidationError("Please complete captcha.")
            correct_text = Fernet(ENC_KEY).decrypt(
                values["opencaptcha_validation"].encode()
            ).decode("utf-8")
            if values["opencaptcha"] != correct_text:
                raise ValidationError("Captcha incorrect, please check twice.")
        res = super(WebsiteForm, self).extract_data(model, values)
        return res
