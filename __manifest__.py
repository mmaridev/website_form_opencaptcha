# Copyright (c) 2020 Marco Marinello <me@marcomarinello.it>
# This file is part of Website - Form OpenCaptcha.
#
# Website - Form OpenCaptcha is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Website - Form OpenCaptcha is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Website - Form OpenCaptcha.  If not, see <http://www.gnu.org/licenses/>.

{
    "name": "Website Form - openCaptcha",
    "summary": "Provides an open-source and non-privacy-aggressive Captcha for Website Forms",
    "version": "0.1",
    "category": "Website",
    "website": "https://gitlab.com/mmaridev/website_form_opencaptcha",
    "author": "Marco Marinello",
    "license": "AGPL-3",
    'installable': True,
    "depends": [
        "website_form",
        "website_crm",
    ],
    "data": [
        #"data/ir_model_data.xml",
        "views/website_crm_template.xml",
    ],
    "external_dependencies": {"python": ["cryptography"]}
}
